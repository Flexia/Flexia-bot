from flask import Flask
from flask_sslify import SSLify
from flask import request
from flask import jsonify
import requests
import json
from models import *
app = Flask(__name__)
#sslify = SSLify(app)

model = KeyedVectors.load_word2vec_format(constants.model_name, binary=True)
tree = build_tree(gen_vec_table(model))



URL = 'https://api.telegram.org/bot483545294:AAELyKZast8NSs_fITyd5tszWaVzoahVqCk/'


def write_json(data, filename='answer.json'):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=2, ensure_ascii=False)


def send_message(chat_id, text='blablabla'):
    url = URL + 'sendMessage'
    answer = {'chat_id': chat_id, 'text' : text}
    r = requests.post(url, json=answer)
    return r.json()



@app.route('/483545294:AAELyKZast8NSs_fITyd5tszWaVzoahVqCk/',methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        r = request.get_json()
        chat_id = r['message']['chat']['id']
        message = r['message']['text']
        vec = gen_quest_vec(message, model)
        answer = get_answer(constants.quest_ans, find_similar(tree, vec))
        send_message(chat_id, text = answer)
        return jsonify(r)
    return '<h1>BOT Wcome U<h1>'




if __name__ == '__main__':
    app.run()
